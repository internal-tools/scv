http = require 'http'

module.exports = (robot) ->
  robot.hear /bd (.*) (.*)/i, (res) ->
    side = res.match[1]
    if side == 'b'
      side = 1
    else
      side = 2
    orderbook_limit = res.match[2]
    robot.http("http://210.112.115.24:8010/calculate_block_deal/1/146/#{side}/#{orderbook_limit}")
      .get() (err, httpRes, body) ->
        response = JSON.parse(body)
        targetPrice = parseFloat(response[0]).toLocaleString()
        totalQuantity = parseInt(response[1]).toLocaleString()
        myOrderQuantity = parseInt(response[2]).toLocaleString()
        otherOrderQuantity = parseInt(response[3]).toLocaleString()
        totalVolume = parseInt(response[4]).toLocaleString()
        myOrderVolume = parseInt(response[5]).toLocaleString()
        otherOrderVolume = parseInt(response[6]).toLocaleString()
        res.send "Target Price: #{targetPrice}\n" +
                 "Total Quantity: #{totalQuantity}\n" +
                 "My Order Quantity: #{myOrderQuantity}\n" +
                 "Other Order Quantity: #{otherOrderQuantity}\n" +
                 "Total Volume: #{totalVolume}\n" +
                 "My Order Volume: #{myOrderVolume}\n" +
                 "Other Order Volume: #{otherOrderVolume}"
